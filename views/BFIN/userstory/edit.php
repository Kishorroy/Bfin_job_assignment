<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;

use App\Userstory\Userstory;

$obj = new Userstory();
$obj->setData($_GET);

$oneData = $obj->view1();


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        Update status
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body style="background-color: green">



<div class="container">


    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: white; background:red; margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="update.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <h2 style="text-align: center"><b> Update User Story</b></h2>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">
                            <div class="form-group">
                                <input type="hidden" class="form-control" value="<?php echo $oneData->id ?>" name="id" >
                                </div>
                            <div class="form-group">
                                <label for="Title" style="color: white">Title</label>
                                <input type="text" class="form-control" value="<?php echo $oneData->title ?>" name="Title" required="">
                            </div>
                            <div class="form-group">
                                <label for="Tag" style="color: white">Tag</label>
                                <input type="text" class="form-control" value="<?php echo $oneData->tag ?>" name="Tag" required="">
                            </div>
                            <div class="form-group">
                                <label for="Body" style="color: white;">Body</label>
                                <input type="text"  class="form-control" value="<?php echo $oneData->body ?>" name="Body" required=""></input>
                            </div>

                            <div class="form-group">
                                <label for="ProfilePicture1" style="color: white">Image</label>
                                <input type="file" class="form-control" name="File2Upload" required="">
                            </div>
                            <div class="form-group">
                                <label for="Caption" style="color: white">Caption for Image</label>
                                <input type="text" class="form-control" name="Caption" value="<?php echo $oneData->caption ?>" required="">
                            </div>

                            <div class="form-group">
                                <label for="Name" style="color: white">Name</label>
                                <input type="text" class="form-control" name="Name" value="<?php echo $oneData->name ?>" required="">
                            </div>
                            <center><button type="submit" class="btn btn-primary">Update User Story</button><br>

                            </center>


                        </div>
                    </div>
                </div>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>



</div>





</body>
</html>
