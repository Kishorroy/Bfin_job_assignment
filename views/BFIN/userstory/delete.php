<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;
use App\Utility\Utility;

use App\Userstory\Userstory;

$obj = new Userstory();
$obj->setData($_GET);

$oneData = $obj->delete();

Utility::redirect("../signup_login/home.php");