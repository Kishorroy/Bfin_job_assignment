<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Signup\Signup;


$obj = new Signup();


$allData  =  $obj->index();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>


    <!-- required for search, block3 of 5 start -->
    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">


    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <!-- required for search, block3 of 5 end -->


</head>

<body >


<div style=" text-align: center;font-size: xx-large;font-family: 'Lucida Calligraphy';color:#2098d1;background: rgba(0,0,0,0.5);padding-top: 30px;">
    <b>User Control Panel</b>
    <br>

</div>



<div class="container">




    <div class="bg-info text-center" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1;"><h1>User Control panel</h1></div>

    <table border="1px" class="table table-bordered table-striped" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1">

        <tr style="background: black">


            <th> ID </th>
            <th> Name </th>
            <th> Email </th>
            <th> Date of birth </th>
            <th> Phone  </th>
            <th> Gender </th>
            <th> Profile Picture </th>
            <th> Action Button </th>

        </tr>


        <form  id="multiple" method="post">

            <?php


             $serial=1;

            foreach ($allData as $oneData){

                if($serial%2) $bgColor = "rgba(0,0,0,0.5)";
                else $bgColor = "rgba(0,0,0,0.5)";

                echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     
    
                                     
                                     <td style='width: 10%; text-align: center'>$oneData->id</td>
                                     <td style='width: 20%;'>$oneData->name</td>
                                     <td style='width: 10%; text-align: center'>$oneData->email</td>
                                     <td style='width: 20%;'>$oneData->dob</td>
                                     <td style='width: 10%; text-align: center'>$oneData->phone</td>
                                     <td style='width: 20%;'>$oneData->gender</td>
                                     <td><a href='../signup_login/view.php?id=$oneData->id'> <img width='100px' height='100px' src='../signup_login/Uploads/$oneData->profile_picture'>  </a>  </td>
    
                                     <td>
                                       
                                       <a href='delete.php?id=$oneData->id' onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                                       
    
                                     </td>
                                  </tr>
                              ";
                $serial++;

            }

            ?>

        </form>
    </table>



</body>
</html>