<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Userstory\Userstory;


$obj = new Userstory();


$allData  =  $obj->index();



?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>


    <!-- required for search, block3 of 5 start -->
    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">


    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>

    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <!-- required for search, block3 of 5 end -->


</head>

<body >


<div style=" text-align: center;font-size: xx-large;font-family: 'Lucida Calligraphy';color:#2098d1;background: rgba(0,0,0,0.5);padding-top: 30px;">
    <b>User Story Control Panel</b>
    <br>

</div>



<div class="container">




    <div class="bg-info text-center" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1;"><h1>User Story Control panel</h1></div>

    <table border="1px" class="table table-bordered table-striped" style="font-family: 'Comic Sans MS'; background: rgba(0,0,0,0.5);color:#2098d1">

        <tr style="background: black">


            <th> Author Name </th>
            <th> User story Title </th>
            <th> Tag </th>
            <th> Body </th>
            <th> User story Image </th>
            <th> Image Caption</th>
            <th> Action Buttons </th>

        </tr>


        <form  id="multiple" method="post">

            <?php


            $serial=1;

            foreach ($allData as $oneData){

                if($serial%2) $bgColor = "rgba(0,0,0,0.5)";
                else $bgColor = "rgba(0,0,0,0.5)";

                echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     
    
                                     
                                     <td style='width: 20%;'>$oneData->name</td>
                                     <td style='width: 20%;'>$oneData->title</td>
                                     <td style='width: 20%;'>$oneData->tag</td>
                                     <td style='width: 20%;'>$oneData->body</td>
                                     <td><a href='../userstory/view.php?id=$oneData->id'> <img width='100px' height='100px' src='../userstory/Uploads/$oneData->profile_picture1'>  </a>  </td>
                                     <td style='width: 20%;'>$oneData->caption</td>
                                     <td>
                                     
                                       
                                       <a href='../userstory/delete.php?id=$oneData->id' onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                                       
    
                                     </td>
                                  </tr>
                              ";
                $serial++;

            }

            ?>

        </form>
    </table>
</body>
</html>