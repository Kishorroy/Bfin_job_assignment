<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin panel</title>
    <style>
        body{
            background: #eee;
        }
        .button {
            background-color: #4CAF50;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
            width: 190px;
        }
        .container{
            border:solid gray 1px;
            width: 20%;
            border-radius: 5px;
            margin: 100px auto;
            background: white;
            padding: 50px;
        }
    </style>
</head>
<body>

<h1><center> Admin Panel</center></h1>
<div class="container">

    <h2> <center>Control Area</center></h2><hr>
    <a href="index1.php" button class="button"><b>User Control Area</b></a></button>
    <a href="index2.php" button class="button"><b>User Story Control Area</b></a></button>
    <a href="login.php" button class="button"><b>Logout</b></a></button>

</div>

</body>
</html>
