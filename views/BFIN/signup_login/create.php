<?php

require_once ("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;
use App\Utility\Utility;
use App\Signup\Signup;


$obj = new Signup();



?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
       Sign up
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body style="background-color: green">

<div id="MessageShowDiv" style="height: 20px; background-color: green;">
    <div id="message" class="btn-danger text-center">
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="container">


    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: white; background:red; margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="store.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <h2 style="text-align: center"><b> Sign up</b></h2>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">
                            <div class="form-group">
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" placeholder="Name" name="Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="Email" required="">
                            </div>
                            <div class="form-group">
                                <label for="Dob">Date of Birth</label>
                                <input type="date" class="form-control" placeholder="Date of Birth" name="Dob" required="">
                            </div>
                            <div class="form-group">
                                <label for="Phone">Phone No</label>
                                <input type="text" class="form-control" placeholder="Phone" name="Phone" required="">
                            </div>
                            <div class="form-group">
                                <label for="Gender">Gender:&nbsp; &nbsp;</label>
                                <input type="radio"  name="Gender" value="male"> Male
                                <input type="radio" name="Gender" value="female"> Female
                            </div>
                            <div class="form-group">
                                <label for="ProfilePicture">Image</label>
                                <input type="file" class="form-control" name="File2Upload" required="">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" class="form-control" name="Password" placeholder="Password" required="">
                            </div>
                            <center><button type="submit" class="btn btn-primary">Submit</button><br>
                            Already have an account?<a href="signin.php" style="color: yellow">Please sign in here</a>
                            </center>

                        </div>
                    </div>
                </div>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>

      

    </div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
