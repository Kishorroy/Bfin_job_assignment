<?php

require_once ("../../../vendor/autoload.php");

use App\Message\Message;

use App\Signup\Signup;

$obj = new Signup();
$obj->setData($_GET);

$oneData = $obj->view();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
       Update profile
    </title>

    <link rel="stylesheet" href="../style.css">

    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>

    <!-- required for search, block3 of 5 start -->

    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>

    <!-- required for search, block3 of 5 end -->



</head>
<body style="background-color: green">

<div id="MessageShowDiv" style="height: 20px; background-color: green;">
    <div id="message" class="btn-danger text-center">
        <?php
        if(isset($_SESSION['message'])){
            echo Message::message();
        }
        ?>
    </div>
</div>

<div class="container">


    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: white; background:red; margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="update.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <h2 style="text-align: center"><b> Update Profile</b></h2>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" value="<?php echo $oneData->id ?>" name="id" >
                                </div>
                                <label for="Name">Name</label>
                                <input type="text" class="form-control" value="<?php echo $oneData->name ?>" name="Name" required="">
                            </div>
                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="email" class="form-control" value="<?php echo $oneData->email ?>" name="Email" required="">
                            </div>
                            <div class="form-group">
                                <label for="Dob">Date of Birth</label>
                                <input type="date" class="form-control" value="<?php echo $oneData->dob ?>" name="Dob" required="">
                            </div>
                            <div class="form-group">
                                <label for="Phone">Phone No</label>
                                <input type="text" class="form-control" value="<?php echo $oneData->phone ?>" name="Phone" required="">
                            </div>
                            <div class="form-group">
                                <label for="Gender">Gender:&nbsp; &nbsp;</label>
                                <input type="radio"  name="Gender" value="male"> Male
                                <input type="radio" name="Gender" value="female"> Female
                            </div>
                            <div class="form-group">
                                <label for="ProfilePicture">Image</label>
                                <input type="file" class="form-control" name="File2Upload" required="">
                            </div>
                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" class="form-control" name="Password" value="<?php echo $oneData->password ?>" required="">
                            </div>
                            <center><button type="submit" class="btn btn-primary">Submit</button><br>

                            </center>

                        </div>
                    </div>
                </div>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>



</div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
