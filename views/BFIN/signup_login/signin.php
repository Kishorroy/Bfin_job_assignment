<?php
session_start();
include ("dashboard.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        Sign in
    </title>



    <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

    <script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>



    <link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
    <script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
    <script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>




</head>
<body style="background-color: green">



<div class="container">

    <div style="background-color: red; color: white; font-size: 25px;"><center><span><?php echo $error; ?></span></center>
    </div>
    <div class="col-sm-3"></div>
    <div class="col-lg-6" style="color: white; background:red; margin-top: 100px;margin-bottom: 150px; border-radius: 10px;padding-top: 10px;padding-bottom: 10px;font-family: 'Comic Sans MS'">


        <form action="" method="post" enctype="multipart/form-data">
            <fieldset>
                <h2 style="text-align: center"><b> Sign in</b></h2>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">

                            <div class="form-group">
                                <label for="Email">Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="Email" required="">
                            </div>


                            <div class="form-group">
                                <label for="Password">Password</label>
                                <input type="password" class="form-control" name="Password" placeholder="Password" required="">
                            </div>
                            <center><button type="submit" class="btn btn-primary" name="signin">Submit</button><br>
                                Don't have an account?<a href="create.php" style="color: yellow">Please sign up here</a><br>

                            </center>

                        </div>
                    </div>
                </div>
            </fieldset>
        </form></div>

    <div class="col-sm-3"></div>


</div>



</div>



<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script>


    $(function ($) {

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);

        $("#message").fadeOut(500);
        $("#message").fadeIn(500);
        $("#message").fadeOut(500);

    });


</script>


</body>
</html>
