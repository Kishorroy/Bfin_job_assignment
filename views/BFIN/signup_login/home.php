<?php
session_start();

require_once ("../../../vendor/autoload.php");

use App\Message\Message;



if(!isset($_SESSION)) session_start();

use App\Utility\Utility;
use App\Userstory\Userstory;


$obj = new Userstory();


$allData  =  $obj->index();

?>
<html>
<title>Profile</title>


<link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">

<script src="../../../resources/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>



<link rel="stylesheet" href="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.css">
<script src="../../../resources/bootstrap-3.3.7-dist/js/jquery-3.2.1.min.js"></script>
<script src="../../../resources/jquery-ui-1.12.1.custom/jquery-ui.js"></script>
<style>
    


</style>


<body style="background-color: lightskyblue">
<div class="container">
    <div class="cover">
        <img src="../../../resources/images/banner/bfin.jpg" style="height: 200px; width: 1100px;"></img>
    </div>
    <br>
    <div>
        <div class="col-lg-2"><img src="Uploads/<?php echo $_SESSION['profile_picture'];?>" style="border: solid; height: 230px; width: 200px;"></center></div>
        <div class="col-lg-9" style=" margin-left: 50px;background-color: #1b6d85; color: white; font-size: 35px; "><center><?php echo $_SESSION['name'];?><hr>
            <div style="font-size: 15px;"><label>Phone No:</label><?php echo $_SESSION['phone'];?><br><label>Date of Birth:</label><?php echo $_SESSION['dob'];?></div><hr>


                <a href="edit.php?id=<?php echo $_SESSION['id']; ?>" class='btn btn-success'>Edit Profile</a>
                <a href="../userstory/Homepage.php" class='btn btn-info'>Home Page</a>
                <a href="logout.php" class='btn btn-danger'>Logout</a>
    </div>
        </center></div><br>


    <div class="status" style="width: 1100px; margin-top: 230px; background-color:#23527c;  "><br>
        <div style="margin-left: 300px;">
    <p style=" margin-left: 30px;margin-top: 15px; color: white; font-size: 20px; font-family: 'AtraiMJ ';">What's On your mind?</p>
        <form action="../userstory/store.php" method="post" enctype="multipart/form-data">
            <fieldset>
                <div class="container">
                    <div class="row" style="padding-bottom: 20px">
                        <div class="col-sm-5" style="padding-left: 75px">
                            <div class="form-group">
                                <label for="Title" style="color: white">Title</label>
                                <input type="text" class="form-control" placeholder="Title" name="Title" required="">
                            </div>
                            <div class="form-group">
                                <label for="Tag" style="color: white">Tag</label>
                                <input type="text" class="form-control" placeholder="Tag" name="Tag" required="">
                            </div>
                            <div class="form-group">
                                <label for="Body" style="color: white;">Body</label>
                                <textarea cols="100" class="form-control" placeholder="Body(less than 100 word)" name="Body" required=""></textarea>
                            </div>

                            <div class="form-group">
                                <label for="ProfilePicture1" style="color: white">Image</label>
                                <input type="file" class="form-control" name="File2Upload" required="">
                            </div>
                            <div class="form-group">
                                <label for="Caption" style="color: white">Caption for Image</label>
                                <input type="text" class="form-control" name="Caption" placeholder="Caption" required="">
                            </div>

                            <div class="form-group">
                                <input type="hidden" class="form-control" name="Name"  value="<?php echo $_SESSION['name'];?>">
                            </div>
                            <center><button type="submit" class="btn btn-primary">Post User Story</button><br>

                            </center>

                        </div>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
    <div class="Show" style="width: 1100px; margin-top: 20px; background-color:#23527c;  "><br>
        <table border="1px" class="table table-bordered table-striped" style=" background: rgba(0,0,0,0.5);color:white">

            <tr style="background: black">

                <th> Author Name </th>
                <th> User story Title </th>
                <th> Tag </th>
                <th> Body </th>
                <th> User story Image </th>
                <th> Image Caption</th>
                <th> Action Buttons </th>

            </tr>


            <form  id="multiple" method="post">

                <?php


                $serial=1;

                foreach ($allData as $oneData){

                    if($serial%2) $bgColor = "rgba(0,0,0,0.5)";
                    else $bgColor = "rgba(0,0,0,0.5)";

                    echo "
    
                                  <tr  style='background-color: $bgColor'>
    
                                     
    
                                     
                                     <td style='width: 20%;'>$oneData->name</td>
                                     <td style='width: 20%;'>$oneData->title</td>
                                     <td style='width: 20%;'>$oneData->tag</td>
                                     <td style='width: 20%;'>$oneData->body</td>
                                     <td><a href='../userstory/view.php?id=$oneData->id'> <img width='100px' height='100px' src='../userstory/Uploads/$oneData->profile_picture1'>  </a>  </td>
                                     <td style='width: 20%;'>$oneData->caption</td>
                                     <td>
                                     
                                       <a href='../userstory/edit.php?id=$oneData->id' class='btn btn-primary'>Edit</a>
                                       <a href='../userstory/delete.php?id=$oneData->id' onclick='return doConfirm()' class='btn btn-danger'>Delete</a>
                                       
    
                                     </td>
                                  </tr>
                              ";
                    $serial++;

                }

                ?>

            </form>
        </table>

    </div>
</div>
</body>


</html>
