<?php
/**
 * Created by PhpStorm.
 * User: kishor
 * Date: 12/26/2017
 * Time: 12:08 AM
 */

namespace App\Userstory;


use App\Model\Database;
use PDO;

class Userstory extends Database
{
    public  $id, $name, $title, $tag, $body, $profilePicture1, $caption;


    public function setData($postArray)
    {

        if (array_key_exists("ID", $postArray))
            $this->id = $postArray["ID"];

        if (array_key_exists("Name", $postArray))
            $this->name = $postArray["Name"];
        if (array_key_exists("Title", $postArray))
            $this->title = $postArray["Title"];
        if (array_key_exists("Tag", $postArray))
            $this->tag = $postArray["Tag"];

        if (array_key_exists("Body", $postArray))
            $this->body = $postArray["Body"];

        if (array_key_exists("ProfilePicture1", $postArray))
            $this->profilePicture1 = $postArray["ProfilePicture1"];
        if (array_key_exists("Caption", $postArray))
            $this->caption = $postArray["Caption"];



    }
    public function store(){

        $sqlQuery = "INSERT INTO userstory ( name,title,tag,body, profile_picture1,caption) VALUES (?,  ?, ?, ?, ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->name,  $this->title,$this->tag, $this->body,   $this->profilePicture1, $this->caption ];

        $status = $sth->execute($dataArray);
    }

    public function view1(){

        $sqlQuery = 'SELECT * FROM userstory WHERE id'.$this->id;

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData=  $sth->fetch();
        return $oneData;

    }
    public function update(){

        $sqlQuery = 'UPDATE userstory SET title=?, tag=?, body=?,id=?,name=?,  profile_picture1=?,caption=? WHERE id'.$this->id;

        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->title,$this->tag,$this->body, $this->id, $this->name, $this->profilePicture1 , $this->caption];

        $status = $sth->execute($dataArray);


    }
    public function index(){

        $sqlQuery = "SELECT * FROM userstory  WHERE is_trashed='NO' ORDER BY id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }
    public function delete(){

        $sqlQuery = 'DELETE FROM userstory where id'.$this->id;

        $status = $this->dbh->exec($sqlQuery);



    }

}
