<?php
namespace App\Signup;

use App\Message\Message;
use App\Model\Database;
use App\Utility\Utility;
use PDO;

class Signup extends Database
{
    public $id, $name,$email,$dob,$phone,$gender, $profilePicture,$password;


    public function setData($postArray){

        if(array_key_exists("id",$postArray))
            $this->id = $postArray["id"];

        if(array_key_exists("Name",$postArray))
            $this->name = $postArray["Name"];
        if(array_key_exists("Email",$postArray))
            $this->email=$postArray["Email"];
        if(array_key_exists("Dob",$postArray))
            $this->dob = $postArray["Dob"];

        if(array_key_exists("Phone",$postArray))
            $this->phone = $postArray["Phone"];
        if(array_key_exists("Gender",$postArray))
            $this->gender=$postArray["Gender"];

        if(array_key_exists("ProfilePicture",$postArray))
            $this->profilePicture = $postArray["ProfilePicture"];
        if(array_key_exists("Password",$postArray))
            $this->password = $postArray["Password"];


    }// end of setData Method


    public function store(){

        $sqlQuery = "INSERT INTO users ( name,email,dob,phone,gender, profile_picture,password) VALUES ( ?, ?, ?, ?, ?, ?, ?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->name, $this->email,$this->dob, $this->phone, $this->gender,  $this->profilePicture, $this->password ];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been inserted successfully<br>");
        else
            Message::message("Failed! Data has not been inserted<br>");

    }// end of store() Method
    public function view(){

        $sqlQuery = "SELECT * FROM users WHERE id=".$this->id;

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $oneData=  $sth->fetch();
        return $oneData;

    }
    public function update(){

        $sqlQuery = "UPDATE users SET name=?, email=?, dob=?, phone=?, gender=?, profile_picture=?,password=? WHERE id=".$this->id;

        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->name,$this->email,$this->dob,$this->phone,$this->gender, $this->profilePicture , $this->password];

        $status = $sth->execute($dataArray);

        if($status)

            Message::message("Success! Data has been updated successfully<br>");
        else
            Message::message("Failed! Data has not been updated<br>");

    }
    public function index(){

        $sqlQuery = "SELECT * FROM users  WHERE is_trashed='NO' ORDER BY id ASC ";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $allData=  $sth->fetchAll();
        return $allData;

    }
    public function delete(){

        $sqlQuery = "DELETE FROM users where id=".$this->id;

        $status = $this->dbh->exec($sqlQuery);





    }

}// end of Signup Class