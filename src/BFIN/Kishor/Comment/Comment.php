<?php
/**
 * Created by PhpStorm.
 * User: kishor
 * Date: 12/26/2017
 * Time: 10:20 PM
 */

namespace App\Comment;


use App\Model\Database;
use PDO;
class Comment extends Database
{
    public $c_id, $comment;
    public function setData($postArray){
        if (array_key_exists("c_id", $postArray))
            $this->c_id = $postArray["c_id"];
        if (array_key_exists("Comment", $postArray))
            $this->comment = $postArray["Comment"];
    }
    public function store(){

        $sqlQuery = "INSERT INTO comment (comment) VALUES (?)";
        $sth = $this->dbh->prepare( $sqlQuery );


        $dataArray = [ $this->comment];

        $status = $sth->execute($dataArray);
    }
    public function index1(){

        $sqlQuery = "SELECT * FROM comment  WHERE is_trashed='NO' ORDER BY c_id DESC";

        $sth = $this->dbh->query($sqlQuery);

        $sth->setFetchMode(PDO::FETCH_OBJ);

        $Data=  $sth->fetchAll();
        return $Data;

    }

}