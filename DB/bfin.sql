-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2017 at 11:33 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bfin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `c_id` int(13) NOT NULL,
  `comment` varchar(1000) NOT NULL,
  `is_trashed` varchar(100) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`c_id`, `comment`, `is_trashed`) VALUES
(1, 'hjkl;lkjdfghjkl;lkjhgfdfghjkl', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(13) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dob` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `profile_picture` varchar(1000) NOT NULL,
  `password` varchar(20) NOT NULL,
  `is_trashed` varchar(10) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `dob`, `phone`, `gender`, `profile_picture`, `password`, `is_trashed`) VALUES
(13, 'Kishor Roy', 'kishorcse37@gmail.com', '1993-01-18', '01832188905', 'male', '151432583410343680_941101889274638_8834414271011331628_n.jpg', '123456', 'NO'),
(14, 'Shajir Haider Alve', 'shajir@gmail.com', '1993-11-05', '01832188915', 'male', '151432651711951294_970997749618385_7805690718933197000_n.jpg', '123456', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `userstory`
--

CREATE TABLE `userstory` (
  `id` int(10) NOT NULL,
  `name` varchar(150) NOT NULL,
  `title` varchar(150) NOT NULL,
  `tag` varchar(100) NOT NULL,
  `body` varchar(5000) NOT NULL,
  `profile_picture1` varchar(150) NOT NULL,
  `caption` varchar(150) NOT NULL,
  `is_trashed` varchar(10) NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userstory`
--

INSERT INTO `userstory` (`id`, `name`, `title`, `tag`, `body`, `profile_picture1`, `caption`, `is_trashed`) VALUES
(30, 'Shajir Haider Alve', 'vjsbjkbfvkjdb', 'vfknndaknkld', 'dfvnkldnalknb', '151432702615319274_331837283869141_5413973243613768273_n.jpg', 'dvkndklankldn', 'NO'),
(31, 'Shajir Haider Alve', 'jvnkjdankj', 'kdnaklndgb', ',vnadkljnbl', '1514327045Bill-Gates.jpg', 'adjbnjlbgnkl', 'NO'),
(32, 'Shajir Haider Alve', 'dnbjljlkankldn', 'dknfaklbn', 'df,mv ,md ', '1514327067mark-zuckerberg.jpg', 'djagnljnlb', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `userstory`
--
ALTER TABLE `userstory`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `c_id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `userstory`
--
ALTER TABLE `userstory`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
